export default {
	// 主题列表
	themeList: [{
			title: '官方',
			name: 'rf',
			color: '#fa436a',
			tabList: [
				'/static/tab/tab-home-rf.png',
				'/static/tab/tab-cate-rf.png',
				'/static/tab/tab-notify-rf.png',
				'/static/tab/tab-my-rf.png'
			]
		},
		{
			title: '嫣红',
			name: 'red',
			color: '#e54d42',
			tabList: [
				'/static/tab/tab-home-red.png',
				'/static/tab/tab-cate-red.png',
				'/static/tab/tab-notify-red.png',
				'/static/tab/tab-my-red.png'
			]
		},
		{
			title: '桔橙',
			name: 'orange',
			color: '#f37b1d',
			tabList: [
				'/static/tab/tab-home-orange.png',
				'/static/tab/tab-cate-orange.png',
				'/static/tab/tab-notify-orange.png',
				'/static/tab/tab-my-orange.png'
			]
		},
		{
			title: '明黄',
			name: 'yellow',
			color: '#fbbd08',
			tabList: [
				'/static/tab/tab-home-yellow.png',
				'/static/tab/tab-cate-yellow.png',
				'/static/tab/tab-notify-yellow.png',
				'/static/tab/tab-my-yellow.png'
			]
		},
		{
			title: '橄榄',
			name: 'olive',
			color: '#8dc63f',
			tabList: [
				'/static/tab/tab-home-olive.png',
				'/static/tab/tab-cate-olive.png',
				'/static/tab/tab-notify-olive.png',
				'/static/tab/tab-my-olive.png'
			]
		},
		{
			title: '森绿',
			name: 'green',
			color: '#39b54a',
			tabList: [
				'/static/tab/tab-home-green.png',
				'/static/tab/tab-cate-green.png',
				'/static/tab/tab-notify-green.png',
				'/static/tab/tab-my-green.png'
			]
		},
		{
			title: '天青',
			name: 'cyan',
			color: '#1cbbb4',
			tabList: [
				'/static/tab/tab-home-cyan.png',
				'/static/tab/tab-cate-cyan.png',
				'/static/tab/tab-notify-cyan.png',
				'/static/tab/tab-my-cyan.png'
			]
		},
		{
			title: '海蓝',
			name: 'blue',
			color: '#0081ff',
			tabList: [
				'/static/tab/tab-home-blue.png',
				'/static/tab/tab-cate-blue.png',
				'/static/tab/tab-notify-blue.png',
				'/static/tab/tab-my-blue.png'
			]
		},
		{
			title: '姹紫',
			name: 'purple',
			color: '#6739b6',
			tabList: [
				'/static/tab/tab-home-purple.png',
				'/static/tab/tab-cate-purple.png',
				'/static/tab/tab-notify-purple.png',
				'/static/tab/tab-my-purple.png'
			]
		},
		{
			title: '木槿',
			name: 'mauve',
			color: '#9c26b0',
			tabList: [
				'/static/tab/tab-home-mauve.png',
				'/static/tab/tab-cate-mauve.png',
				'/static/tab/tab-notify-mauve.png',
				'/static/tab/tab-my-mauve.png'
			]
		},
		{
			title: '桃粉',
			name: 'pink',
			color: '#e03997',
			tabList: [
				'/static/tab/tab-home-pink.png',
				'/static/tab/tab-cate-pink.png',
				'/static/tab/tab-notify-pink.png',
				'/static/tab/tab-my-pink.png'
			]
		},
		{
			title: '棕褐',
			name: 'brown',
			color: '#a5673f',
			tabList: [
				'/static/tab/tab-home-brown.png',
				'/static/tab/tab-cate-brown.png',
				'/static/tab/tab-notify-brown.png',
				'/static/tab/tab-my-brown.png'
			]
		},
		{
			title: '玄灰',
			name: 'grey',
			color: '#8799a3',
			tabList: [
				'/static/tab/tab-home-grey.png',
				'/static/tab/tab-cate-grey.png',
				'/static/tab/tab-notify-grey.png',
				'/static/tab/tab-my-grey.png'
			]
		},
		{
			title: '草灰',
			name: 'gray',
			color: '#aaaaaa',
			tabList: [
				'/static/tab/tab-home-gray.png',
				'/static/tab/tab-cate-gray.png',
				'/static/tab/tab-notify-gray.png',
				'/static/tab/tab-my-gray.png'
			]
		},
		{
			title: '墨黑',
			name: 'black',
			color: '#333333',
			tabList: [
				'/static/tab/tab-home-black.png',
				'/static/tab/tab-cate-black.png',
				'/static/tab/tab-notify-black.png',
				'/static/tab/tab-my-black.png'
			]
		}
	]
	
	,
	/* 购物车 */
	cartList :[
		{
		firmId: 1,
		firmName: '阿巴阿巴',
		selectedAll: false,
		goods: [{
				id: 1,
				image: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2319343996,1107396922&fm=26&gp=0.jpg',
				attr_val: '春装款 L',
				stock: 15,
				title: 'OVBE 长袖风衣',
				price: 278.00,
				selected: false,
				number: 1
			},
			{
				id: 3,
				image: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2319343996,1107396922&fm=26&gp=0.jpg',
				attr_val: '激光导航 扫拖一体',
				stock: 3,
				title: '科沃斯 Ecovacs 扫地机器人',
				price: 1348.00,
				selected: false,
				number: 5
			},
			{
				id: 4,
				image: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2668268226,1765897385&fm=26&gp=0.jpg',
				attr_val: 'XL',
				stock: 55,
				title: '朵绒菲小西装',
				price: 175.88,
				selected: false,
				number: 1
			},
			{
				id: 5,
				image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1552410549432&di=06dd3758053fb6d6362516f30a42d055&imgtype=0&src=http%3A%2F%2Fimgcache.mysodao.com%2Fimg3%2FM0A%2F67%2F42%2FCgAPD1vNSsHNm-TnAAEy61txQb4543_400x400x2.JPG',
				attr_val: '520 #粉红色',
				stock: 15,
				title: '迪奥（Dior）烈艳唇膏',
				price: 1089.00,
				selected: false,
				number: 1
			},
			{
				id: 6,
				image: 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1031875829,2994442603&fm=26&gp=0.jpg',
				attr_val: '樱花味润手霜 30ml',
				stock: 15,
				title: "欧舒丹（L'OCCITANE）乳木果",
				price: 128,
				selected: false,
				number: 1
			},
			{
				id: 7,
				image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1553007107&di=390915aa8a022cf0b03c03340881b0e7&imgtype=jpg&er=1&src=http%3A%2F%2Fimg13.360buyimg.com%2Fn0%2Fjfs%2Ft646%2F285%2F736444951%2F480473%2Faa701c97%2F548176feN10c9ed7b.jpg',
				attr_val: '特级 12个',
				stock: 7,
				title: '新疆阿克苏苹果 特级',
				price: 58.8,
				selected: false,
				number: 10
			},
			{
				id: 8,
				image: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2319343996,1107396922&fm=26&gp=0.jpg',
				attr_val: '激光导航 扫拖一体',
				stock: 15,
				title: '科沃斯 Ecovacs 扫地机器人',
				price: 1348.00,
				selected: false,
				number: 1
			},
			{
				id: 9,
				image: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2668268226,1765897385&fm=26&gp=0.jpg',
				attr_val: 'XL',
				stock: 55,
				title: '朵绒菲小西装',
				price: 175.88,
				selected: false,
				number: 1
			},
			{
				id: 10,
				image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1552410549432&di=06dd3758053fb6d6362516f30a42d055&imgtype=0&src=http%3A%2F%2Fimgcache.mysodao.com%2Fimg3%2FM0A%2F67%2F42%2FCgAPD1vNSsHNm-TnAAEy61txQb4543_400x400x2.JPG',
				attr_val: '520 #粉红色',
				stock: 15,
				title: '迪奥（Dior）烈艳唇膏',
				price: 1089.00,
				selected: false,
				number: 1
			},
			{
				id: 11,
				image: 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1031875829,2994442603&fm=26&gp=0.jpg',
				attr_val: '樱花味润手霜 30ml',
				stock: 15,
				title: "欧舒丹（L'OCCITANE）乳木果",
				price: 128,
				selected: false,
				number: 1
			},
			{
				id: 12,
				image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1553007107&di=390915aa8a022cf0b03c03340881b0e7&imgtype=jpg&er=1&src=http%3A%2F%2Fimg13.360buyimg.com%2Fn0%2Fjfs%2Ft646%2F285%2F736444951%2F480473%2Faa701c97%2F548176feN10c9ed7b.jpg',
				attr_val: '特级 12个',
				stock: 7,
				title: '新疆阿克苏苹果 特级',
				price: 58.8,
				selected: false,
				number: 10
			},
			{
				id: 13,
				image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1552405266625&di=a703f2b2cdb0fe7f3f05f62dd91307ab&imgtype=0&src=http%3A%2F%2Fwww.78.cn%2Fzixun%2Fnews%2Fupload%2F20190214%2F1550114706486250.jpg',
				attr_val: '春装款/m',
				stock: 15,
				title: '女装2019春秋新款',
				price: 420.00,
				selected: false,
				number: 1
	
			}
		],
		},{
		firmId: 2,
		firmName: '萨拉嘿',
		selectedAll: false,
		goods: [{
				id: 1,
				image: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2319343996,1107396922&fm=26&gp=0.jpg',
				attr_val: '春装款 L',
				stock: 15,
				title: 'OVBE 长袖风衣',
				price: 278.00,
				selected: false,
				number: 1
			},
			{
				id: 3,
				image: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2319343996,1107396922&fm=26&gp=0.jpg',
				attr_val: '激光导航 扫拖一体',
				stock: 3,
				title: '科沃斯 Ecovacs 扫地机器人',
				price: 1348.00,
				selected: false,
				number: 5
			},
			{
				id: 4,
				image: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2668268226,1765897385&fm=26&gp=0.jpg',
				attr_val: 'XL',
				stock: 55,
				title: '朵绒菲小西装',
				price: 175.88,
				selected: false,
				number: 1
			},
			{
				id: 5,
				image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1552410549432&di=06dd3758053fb6d6362516f30a42d055&imgtype=0&src=http%3A%2F%2Fimgcache.mysodao.com%2Fimg3%2FM0A%2F67%2F42%2FCgAPD1vNSsHNm-TnAAEy61txQb4543_400x400x2.JPG',
				attr_val: '520 #粉红色',
				stock: 15,
				title: '迪奥（Dior）烈艳唇膏',
				price: 1089.00,
				selected: false,
				number: 1
			},
			{
				id: 6,
				image: 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1031875829,2994442603&fm=26&gp=0.jpg',
				attr_val: '樱花味润手霜 30ml',
				stock: 15,
				title: "欧舒丹（L'OCCITANE）乳木果",
				price: 128,
				selected: false,
				number: 1
			},
			{
				id: 7,
				image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1553007107&di=390915aa8a022cf0b03c03340881b0e7&imgtype=jpg&er=1&src=http%3A%2F%2Fimg13.360buyimg.com%2Fn0%2Fjfs%2Ft646%2F285%2F736444951%2F480473%2Faa701c97%2F548176feN10c9ed7b.jpg',
				attr_val: '特级 12个',
				stock: 7,
				title: '新疆阿克苏苹果 特级',
				price: 58.8,
				selected: false,
				number: 10
			},
			{
				id: 8,
				image: 'https://ss2.bdstatic.com/70cFvnSh_Q1YnxGkpoWK1HF6hhy/it/u=2319343996,1107396922&fm=26&gp=0.jpg',
				attr_val: '激光导航 扫拖一体',
				stock: 15,
				title: '科沃斯 Ecovacs 扫地机器人',
				price: 1348.00,
				selected: false,
				number: 1
			},
			{
				id: 9,
				image: 'https://ss1.bdstatic.com/70cFvXSh_Q1YnxGkpoWK1HF6hhy/it/u=2668268226,1765897385&fm=26&gp=0.jpg',
				attr_val: 'XL',
				stock: 55,
				title: '朵绒菲小西装',
				price: 175.88,
				selected: false,
				number: 1
			},
			{
				id: 10,
				image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1552410549432&di=06dd3758053fb6d6362516f30a42d055&imgtype=0&src=http%3A%2F%2Fimgcache.mysodao.com%2Fimg3%2FM0A%2F67%2F42%2FCgAPD1vNSsHNm-TnAAEy61txQb4543_400x400x2.JPG',
				attr_val: '520 #粉红色',
				stock: 15,
				title: '迪奥（Dior）烈艳唇膏',
				price: 1089.00,
				selected: false,
				number: 1
			},
			{
				id: 11,
				image: 'https://ss0.bdstatic.com/70cFvHSh_Q1YnxGkpoWK1HF6hhy/it/u=1031875829,2994442603&fm=26&gp=0.jpg',
				attr_val: '樱花味润手霜 30ml',
				stock: 15,
				title: "欧舒丹（L'OCCITANE）乳木果",
				price: 128,
				selected: false,
				number: 1
			},
			{
				id: 12,
				image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1553007107&di=390915aa8a022cf0b03c03340881b0e7&imgtype=jpg&er=1&src=http%3A%2F%2Fimg13.360buyimg.com%2Fn0%2Fjfs%2Ft646%2F285%2F736444951%2F480473%2Faa701c97%2F548176feN10c9ed7b.jpg',
				attr_val: '特级 12个',
				stock: 7,
				title: '新疆阿克苏苹果 特级',
				price: 58.8,
				selected: false,
				number: 10
			},
			{
				id: 13,
				image: 'https://timgsa.baidu.com/timg?image&quality=80&size=b9999_10000&sec=1552405266625&di=a703f2b2cdb0fe7f3f05f62dd91307ab&imgtype=0&src=http%3A%2F%2Fwww.78.cn%2Fzixun%2Fnews%2Fupload%2F20190214%2F1550114706486250.jpg',
				attr_val: '春装款/m',
				stock: 15,
				title: '女装2019春秋新款',
				price: 420.00,
				selected: false,
				number: 1
		
			}
		],
	}]
	


};
