/**
 * 全局剪贴板
 * Created by gaobo on 2020/08/08
 */
import Toast from "@/common/toast.js"
// #ifdef H5
import ClipboardJS from "@/components/clipboard.min.js"
// #endif
let flag = false   // 是否复制成功
function copyToClip(e){
	var Context = plus.android.importClass("android.content.Context");  
	var main = plus.android.runtimeMainActivity();  
	var clip = main.getSystemService(Context.CLIPBOARD_SERVICE);  
	plus.android.invoke(clip,"setText",e);
	flag = true
}
export default {
	/**
	 * 
	 * 公共组件 复制
	 * 
	 */
	
	onCopy:(data,callback,e)=>{
		// #ifdef APP-PLUS
		var os = plus.os.name;
		if(data){
			if(os == "iOS"){
				var UIPasteboard  = plus.ios.importClass("UIPasteboard");  
				var generalPasteboard = UIPasteboard.generalPasteboard();  
				// 设置/获取文本内容:  
				generalPasteboard.setValueforPasteboardType("testValue", "public.utf8-plain-text");  
				var value = generalPasteboard.valueForPasteboardType("public.utf8-plain-text");
				flag = true
			}else{
				copyToClip(data)
			}
		}
		// #endif
		// #ifdef H5
		let event = window.event || e || {}
		console.log(event)
		let clipboard = new ClipboardJS("", {text: () => data})
		clipboard.on('success', (e) => {
			("function" == typeof callback) && callback(true)
			clipboard.off('success')
			clipboard.off('error')
			clipboard.destroy()
			flag = true
		});
		clipboard.on('error', (e) => {
			("function" == typeof callback) && callback(false)
			clipboard.off('success')
			clipboard.off('error')
			clipboard.destroy()
			flag = false
		});
		clipboard.onClick(event)
		// #endif
		return flag
	},
	
	
		
}
