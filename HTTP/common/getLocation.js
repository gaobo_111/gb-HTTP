import modal from "./modal.js"
export default{
	// 获取地理位置
	location:(()=>{
		return new Promise((resolve, reject) => {
			uni.getLocation({
				type: 'wgs84',
				geocode:true,
				success: function (res) {
					console.log(res)
					resolve(res)
				},
				fail: function(err) {
					// console.log(err); // 用户未开启GPS，可能获取不到
					reject("获取定位失败，是否授权打开定位")
					modal.showModal({content: `获取定位失败，是否授权打开定位`}).then((data) => {
						uni.getSystemInfo({
							success: (sys) => {
								if(sys.platform=='ios'){
									plus.runtime.openURL("app-settings://");
								}else{
									var main = plus.android.runtimeMainActivity();
									var Intent = plus.android.importClass("android.content.Intent");
									//可能应该直接进入应用列表的权限设置？=> android.settings.APPLICATION_SETTINGS
									var mIntent = new Intent('android.settings.LOCATION_SOURCE_SETTINGS');
									main.startActivity(mIntent);
								}
							}
						})
					}).catch((reject) => {
						
					});
				}
			});
		});
		
	})
}
