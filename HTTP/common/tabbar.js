/**
 * 自定义tabbar
 * Created by gaobo on 2020/10/27
 */
import cache from './cache.js'
export default{
	/**
	 * tabbar
	 * 未点击状态iconData
	 * 点击状态selectedData
	 */
	tabbar:(iconData,selectedData)=>{
		var that = this
		// 点击
		let selectedDataArr = []  //临时存储点击状态下的图片、
		let permanentSelectedArr = [] // //永久存储已经排序点击状态下的图片
		let selectedStorage = ["1","2","3","4"] // 存储下载好路径 （提前写好内容为了能够指定顺序更换）
		// 填入点击状态需要被查找的关键词
		var str = "shouye";
		var str1 = "zuhao";
		var str2 = "fahao";
		var str3 = "xiaoxi";
		// var str4 = "wode";
		// 未点击
		let iconDataArr = []  //临时存储未点击状态下的图片
		let permanentIconDatadArr = [] // //永久存储已经排序未点击状态下的图片
		let iconStorage = ["1","2","3","4"] // 存储下载好路径 （提前写好内容为了能够指定顺序更换）
		// 填入未点击状态需要被查找的关键词
		var nostr = "808a26819ee5418d89afd9351ea040f3";
		var nostr1 = "a751fbd26b4d4cde97844f2ac0ac8d3f";
		var nostr2 = "e060d5ac23694ccdb32d22702c011bfb";
		var nostr3 = "2eae03580dc5492f95d3e87cfc1fee9d";
		// 点击状态
		for(var i=0;i<selectedData.length;i++){
			uni.downloadFile({
				url: selectedData[i],//下载地址接口返回
				success: (data) => {
					if (data.statusCode === 200) {
						selectedDataArr.push(data.tempFilePath)
						if(selectedDataArr.length == selectedData.length){
							// 排序
							for(let x=0;x < selectedDataArr.length;x++){
								if(selectedDataArr[x].indexOf(str) != -1){
									selectedStorage.splice(0,1,selectedDataArr[x])
								}
								if(selectedDataArr[x].indexOf(str1) != -1){
									selectedStorage.splice(1,1,selectedDataArr[x])
								}
								if(selectedDataArr[x].indexOf(str2) != -1){
									selectedStorage.splice(2,1,selectedDataArr[x])
								}
								if(selectedDataArr[x].indexOf(str3) != -1){
									selectedStorage.splice(3,1,selectedDataArr[x])
								}
							}
							// 将排序好的数组循环到指定的tabbar
							for(let j=0;j<selectedStorage.length;j++){
								uni.saveFile({
									tempFilePath:selectedStorage[j], //临时路径
									success: function(res) {
										permanentSelectedArr.push(res.savedFilePath)
										if(permanentSelectedArr.length == selectedStorage.length){
											for(let l=0;l<permanentSelectedArr.length;l++){
												uni.setTabBarItem({
												  index: l,
												  selectedIconPath: permanentSelectedArr[l]
												})
												if(l + 1 == permanentSelectedArr.length){
													console.log(permanentSelectedArr)
													cache.updateCache(cache.CLICKSTATUS,permanentSelectedArr) // 缓存保存临时路径
												}
											}
										}
										
									}
								});
							}
						}
					}
				},
			});
		}
		// 未点击状态
		for(var i=0;i<iconData.length;i++){
			uni.downloadFile({
				url: iconData[i],//下载地址接口返回
				success: (data) => {
					if (data.statusCode === 200) {
						iconDataArr.push(data.tempFilePath)
						if(iconDataArr.length == iconData.length){
							// 排序
							for(let x=0;x < iconDataArr.length;x++){
								if(iconDataArr[x].indexOf(nostr) != -1){
									iconStorage.splice(0,1,iconDataArr[x])
								}
								if(iconDataArr[x].indexOf(nostr1) != -1){
									iconStorage.splice(1,1,iconDataArr[x])
								}
								if(iconDataArr[x].indexOf(nostr2) != -1){
									iconStorage.splice(2,1,iconDataArr[x])
								}
								if(iconDataArr[x].indexOf(nostr3) != -1){
									iconStorage.splice(3,1,iconDataArr[x])
								}
							}
							// 将排序好的数组循环到指定的tabbar
							for(let j=0;j<iconStorage.length;j++){
								uni.saveFile({
									tempFilePath:iconStorage[j], //临时路径
									success: function(res) {
										permanentIconDatadArr.push(res.savedFilePath)
										if(permanentIconDatadArr.length == iconStorage.length){
											for(let l=0;l<permanentIconDatadArr.length;l++){
												uni.setTabBarItem({
												  index: l,
												  iconPath: permanentIconDatadArr[l]
												})
												
												if(l + 1 == permanentIconDatadArr.length){
													cache.updateCache(cache.NOCLICKSTATUS,permanentIconDatadArr) // 缓存保存临时路径
												}
											}
										}
										
									}
								});
							}
						}
					}
				},
			});
		}
	},
	// 已经存在了路径在赋值
	aglinTabbar:(e,d)=>{
		for(var i=0;i<e.length;i++){
			uni.setTabBarItem({
			  index: i,
			  iconPath: e[i]
			})
		}
		for(var j=0;j<d.length;j++){
			uni.setTabBarItem({
			  index: j,
			  selectedIconPath: d[j]
			})
		}
	}
}