 /**
  * 全局公共方法
  * Created by gaobo on 2020/10/27
  */
 import api from '../http/api.js'
 export default {
	// 计算缓存大小
	byteConvert: function(bytes) {
		if (isNaN(bytes)) {
			return '';
		}
		var symbols = ['bytes', 'KB', 'MB', 'GB', 'TB', 'PB', 'EB', 'ZB', 'YB'];
		var exp = Math.floor(Math.log(bytes)/Math.log(2));
		if (exp < 1) {
			exp = 0;
		}
		var i = Math.floor(exp / 10);
		bytes = bytes / Math.pow(2, 10 * i);

		if (bytes.toString().length > bytes.toFixed(2).toString().length) {
			bytes = bytes.toFixed(2);
		}
		return bytes + ' ' + symbols[i];
	},
	// 转换时间戳 年月日时分
	getLocalTime:(date)=>{
		var date = new Date(date*1000);
		var Y = date.getFullYear() + '-';
		var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
		var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
		var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
		var m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes()) + ':';
		var s = (date.getSeconds() <10 ? '0' + date.getSeconds() : date.getSeconds());
		return Y+M+D+h+m+s;
	},
	// 转换时间戳 年月日
	getLocalTimeFullYear:(date)=>{
		var date = new Date(date*1000);
		var Y = date.getFullYear() + '-';
		var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
		var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
		return Y+M+D;
	},
	// 转换时间戳 月日时分
	getLocalTimeMonth:(date)=>{
		var date = new Date(date*1000);
		var M = (date.getMonth()+1 < 10 ? '0'+(date.getMonth()+1) : date.getMonth()+1) + '-';
		var D = (date.getDate() < 10 ? '0' + (date.getDate()) : date.getDate()) + ' ';
		var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
		var m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes());
		return M+D+h+m;
	},
	// 小时/秒
	getHours:(date)=>{
		var date = new Date(date*1000);
		var h = (date.getHours() < 10 ? '0' + date.getHours() : date.getHours()) + ':';
		var m = (date.getMinutes() <10 ? '0' + date.getMinutes() : date.getMinutes());
		return h+m;
	},
	// 天
	getDate:(type)=>{
		const date = new Date();
		let year = date.getFullYear();
		let month = date.getMonth() + 1;
		let day = date.getDate();
		month = month > 9 ? month : '0' + month;
		day = day > 9 ? day : '0' + day;
		return `${year}-${month}-${day}`;
	},
	// 获取月份
	getMonth:(time)=>{
		var date = new Date(time * 1000);
		var M = (date.getMonth() + 1) + '月';
		return M;
	},
	// 正则表达式验证手机号格式是否正确
		isPhoneAvailable: function(phonevalue){
		   let phoneReg = /^1[3-9]\d{9}$/;
		   if(phoneReg.test(phonevalue)){
			   return true;
		   }else{
			   return false;
		   }
	},
	// 获取设备信息
	getDeviceInfo:()=>{
		let info = {
			imei: '',
			model: '',
		}
		try {
			const res = uni.getSystemInfoSync();
			info.model = res.model
			let imei = ''
			// #ifdef APP-PLUS
			let Context = plus.android.importClass("android.content.Context");
			let tephoneManager = plus.android.importClass("android.telephony.TelephonyManager");
			let tm = plus.android.runtimeMainActivity().getSystemService(Context.TELEPHONY_SERVICE);
			imei = tm.getDeviceId();
			info.imei = imei   //获取的安卓imei 返回给info
			// #endif	
			// TODO 此处是安卓的IMEI,iOS获取设备信息参考 http://www.html5plus.org/doc/zh_cn/device.html
			// plus.device.getInfo({
			// 	success:function(e){
			// 		console.log('getDeviceInfo success: ' + JSON.stringify(JSON.stringify(e)));
			// 	},
			// 	fail:function(e){
			// 		console.log('getDeviceInfo failed: ' + JSON.stringify(JSON.stringify(e)));
			// 	}
			// });
			// info.imei = res.imei
		} catch (e) { console.log(e) }
		return info;
	},
	/**
	 * 计算金额
	 * <p>向下兼容</p>
	 * <p>money(A, B, method, digit)</p>
	 * <ul>
	 * 	<li>A：要计算的第一个数</li>
	 * 	<li>B：要计算的第二个数</li>
	 * 	<li>method：计算方式，String类型，规定值 + - * / </li>
	 * 	<li>digit：保留几位小数，默认为2</li>
	 * </ul>
	 */
	money(A, B, method, digit){
		return this.calculate(A, B, method, digit);
	},
	/**
	 * 对两个数进行加减乘除的简单运算
	 * <p>calculate(A, B, method, digit)</p>
	 * <ul>
	 * 	<li>A：要计算的第一个数</li>
	 * 	<li>B：要计算的第二个数</li>
	 * 	<li>method：计算方式，String类型，规定值 + - * / </li>
	 * 	<li>digit：保留几位小数，默认为2</li>
	 * </ul>
	 */
	calculate(A, B, method, digit){
		let result = null
		// 如果不规定保留几位小数默认保留2位
		if(!digit)digit = 2
		// 如果小于0，默认保留2位
		if(digit < 0) {
			digit = 2;
		}
		switch (method){
			case '+':
				return result = parseFloat(A+B).toFixed(digit)
				break;
			case '-':
				return result = parseFloat(A-B).toFixed(digit)
				break;
			case '*':
				return result = parseFloat(A*B).toFixed(digit)
				break;
			case '/':
				return result = parseFloat(A/B).toFixed(digit)
				break;
			default:
				return "计算错误"
				break;
		}
	},
	/**
	 * 对多个数进行加减乘除的简单运算并保留2位小数
	 * <p>calculate(method, ...numbers)</p>
	 * <p>使用方式：calculate("+", 1, 2, 3, 4)，代表计算 1+2+3+4 并保留2位小数</p>
	 * <ul>
	 * 	<li>digit：保留几位小数，默认为2</li>
	 * 	<li>method：计算方式，String类型，规定值 + - * / </li>
	 * 	<li>numbers：需要计算的多个数，传值方式为 1,2,3,4</li>
	 * </ul>
	 */
	calculates(method, ...numbers){
		return this.calculatesRetain(2, method, numbers);
	},
	/**
	 * 对多个数进行加减乘除的简单运算并保留指定位数小数
	 * <p>calculatesRetain(digit, method, ...numbers)</p>
	 * <p>使用方式：calculatesRetain(2, "+", 1, 2, 3, 4)，代表计算 1+2+3+4 并保留2位小数</p>
	 * <ul>
	 * 	<li>digit：保留几位小数，默认为不保留小数</li>
	 * 	<li>method：计算方式，String类型，规定值 + - * / </li>
	 * 	<li>numbers：需要计算的多个数，传值方式为 1,2,3,4</li>
	 * </ul>
	 */
	calculatesRetain(digit, method, ...numbers){
		// 如果不规定保留几位小数默认不保留小数
		if(!digit)digit = 0
		// 如果小于0，默认不保留小数
		if(digit < 0) {
			digit = 0;
		}
		// 初始返回值为0
		let result = 0;
		/*
		 * 处理要遍历的数组
		 */
		let forNumbers = [];
		// 如果参数数组为二位数组，说明是从另一个函数尽来的
		if(numbers[0][0] != null) {
			// 取出真正需要计算的数组
			forNumbers = numbers[0];
		} else {
			// 如果参数数组不是二位数组，那么参数数组就是真正需要遍历的数组
			forNumbers = numbers;
		}
		/*
		 * 遍历数组，进行计算
		 */
		for (let index in forNumbers) {
			/*
			 * 第一次循环，取出第一个数
			 */
			if (index == 0) {
				result = forNumbers[0] * 1;
				continue;
			}
			/*
			 * 从第二次循环开始，进行计算
			 */
			// 取出计算的数，并乘1，以确保为数值型
			var num = forNumbers[index] * 1;
			// 进行计算
			switch (method){
				case '+':
					result = parseFloat(result + num)
					break;
				case '-':
					result = parseFloat(result - num)
					break;
				case '*':
					result = parseFloat(result * num)
					break;
				case '/':
					result = parseFloat(result / num)
					break;
				default:
					return "计算错误"
					break;
			}
			// 计算的结果乘1，以确保为数值型
			result = result * 1;
		}
		// 最后返回计算结果时再保留两位小数，确保计算结果的准确性
		return result.toFixed(digit);
	},
	objSort(prop) { //根据数组某一属性排序 Array = Array.sort(this.objSort('id')) //按照id排序并赋值
		return function(obj1, obj2) {
			var val1 = obj1[prop];
			var val2 = obj2[prop];
			if (!isNaN(Number(val1)) && !isNaN(Number(val2))) {
				val1 = Number(val1);
				val2 = Number(val2);
			}
			if (val1 < val2) {
				return -1;
			} else if (val1 > val2) {
				return 1;
			} else {
				return 0;
			}
		}
	},
	/**
	* 常用方法
	* Created by wangyahui on 2020/11/7
	*/
	//数组复制
	copyArr(arr){
		return JSON.parse(JSON.stringify(arr))
	},
	//返回上一页判断是否是最后一页
	Goback(){
		if (window.history.length <= 1) {
			this.$router.push({path:'/'})
			return false
		} else {
			this.$router.go(-1)
		}
		//上面都没执行就说明卡在当前页不是最后一条， histroy记录数量大于1，又没有回退记录，只能返回首页，
	 //如果上面都执行了 页面都跳走了，这个也就不用管了   
		setTimeout(() => {
			this.$router.push({path:'/'})      
		},500)
	},
	//返回上一页携带参数
	prePage(index) {
		let pages = getCurrentPages();
		let prePage = pages[pages.length - (index || 2)];
		// #ifdef H5
		return prePage;
		// #endif
		return prePage.$vm;
	},
	//开发环境全局打印日志
	log(title) {
		if (process.env.NODE_ENV === 'development' && Boolean(title) === true) {
			console.log(JSON.stringify(title));
		}
	},
	/**
	 * 获取一个随机数
	 * @param {Object} min
	 * @param {Object} max
	 */
	random(min, max) {
		switch (arguments.length) {
			case 1:
				return parseInt(Math.random() * min + 1, 10);
				break;
			case 2:
				return parseInt(Math.random() * (max - min + 1) + min, 10);
				break;
			default:
				return 0;
				break;
		}
	},
	// 判断两个对象是否相同
	isObjectValueEqual(x, y) {
		// 指向同一内存时
		if (x === y) {
			return true;
		} else if (
			typeof x == 'object' &&
			x != null &&
			typeof y == 'object' && y != null
		) {
			if (Object.keys(x).length != Object.keys(y).length) return false;
	
			for (var prop in x) {
				if (y.hasOwnProperty(prop)) {
					if (!this.isObjectValueEqual(x[prop], y[prop])) return false;
				} else return false;
			}
	
			return true;
		} else return false;
	},
	// 去掉字符串中的空格
	trim(str) {
		if (!str) {
			return '';
		}
		return str.replace(/\s*/g, '');
	},
	// 解决js计算精度丢失问题 accMul(1+1)
	accMul(f, digit) {
		if(digit == undefined) digit = 2
		var m = Math.pow(10, digit);
		return Math.round(f * m, 10) / m;
	},
	//保留两位小数不四舍五入
	Decimal(x) {
		var m = Math.pow(10, 2);
		var hshs = Math.round((x * 100) * m, 10) / m;
		console.log(hshs)
		var f_x = Math.round((hshs / 100 )* m) / m;
		var s_x = f_x.toString();
		var pos_decimal = s_x.indexOf('.');
		if (pos_decimal < 0) {
			pos_decimal = s_x.length;
			s_x += '.';
		}
		while (s_x.length <= pos_decimal + 2) {
			s_x += '0';
		}
		return s_x;
	},
	/*
	 * obj 转 params字符串参数
	 * 例子：{a:1,b:2} => a=1&b=2
	 */
	objParseParam(obj) {
		let paramsStr = '';
		if (obj instanceof Array) return paramsStr;
		if (!(obj instanceof Object)) return paramsStr;
		for (let key in obj) {
			paramsStr += `${key}=${obj[key]}&`;
		}
		return paramsStr.substring(0, paramsStr.length - 1);
	},
	/**
	 * 图片处理-选择
	 * count 数量
	 */
	chooseImage(count) {
		return new Promise((resolve, reject) => {
			uni.chooseImage({
				count: count || 1, //默认1
				sourceType: ["camera", "album"],
				sizeType: ["compressed"],  //选择图片
				success: res => {
					resolve(res);
				}
			});
		}).catch(e => {
			reject(e)
		})
	},
	/**
	 * 图片处理-预览
	 * loop 
	 * current
	 * urls
	 */
	previewImage(current,urls,loop) {
		uni.previewImage({
			current: current || 0,
			loop:loop || true,// 是否可以循环预览
			urls: urls || []  
		})
	},
	/**
	 * 图片处理-多文件上传
	 * path 上传文件
	 * header token
	 * url  上传路径
	 */
	uploadFile(path,url){
		let uploadFiles = []
		path.forEach((item, index) => {
			uploadFiles.push({
				uri: item,
				name: 'file' + index
			});
		})
		console.log()
		let token = cache.fetchCache(cache.TOKEN) || ""
		return new Promise((resolve, reject) => {
			uni.uploadFile({
				header:{'token':token},
				url:url,
				files:uploadFiles, 
				formData: {
				},
				success: (uploadFileRes) => {
					console.log(uploadFileRes)
				},
				fail(e) {
					console.log(e)
					reject(e)
				},
				complete(res) {
					
				}
			});
		}).catch(e => {
			// reject(e)
			console.log(e)
		})
		
	},
	/**
	 * 文件上传  ----- 阿里云上传所需的签名
	 * path 上传文件
	 * header token
	 * url  上传路径 
	 */
	getAliyunOssSign(imageList,fileName){
		return new Promise((resolve, reject) => {
			api.getAliyunOssSign().then(res => {
				console.log(res)
				const {code,data,msg,time} = res.data
				if(code === 1){
					  // OSS地址
					  const aliyunServerURL = data.host
					  // 存储路径(后台固定位置+随即数+文件格式)
					  const aliyunFileKey = data.dir + new Date().getTime() + Math.floor(Math.random() * 100) + fileName
					  // 临时AccessKeyID0
					  const OSSAccessKeyId = data.accessid
					  // 加密策略
					  const policy = data.policy
					  // 签名
					  const signature = data.signature
					  // 后端自定义回调
					  const callback = data.callback
					  uni.uploadFile({
						url: aliyunServerURL,
						filePath: imageList,//要上传文件资源的路径
						name: 'file',//必须填file
						formData: {
						  'key': aliyunFileKey,
						  'policy': policy,
						  'callback':callback,
						  'OSSAccessKeyId': OSSAccessKeyId,
						  'signature': signature,
						  'success_action_status': '200',
						},
						success: function (res) {
							resolve(JSON.parse(res.data))
							 
						},
						fail: function (err) {
							resolve(err)
						},
					  })
				}else{
				}
	        }).catch((err) => {
				console.log(err)
			})
		}).catch(e => {
			// reject(e)
			console.log(e)
		})
	},
	/**
	 * 文件下载临时路径
	 * url     string    // 下载地址  
	 */
	uni_downloadFile(url){
		uni.showLoading({
			title:"正在下载中"
		})
		var downloadTask = uni.downloadFile({
		    url: url,
			header:{},
		    success: (res) => {
				console.log(res)
		        if (res.statusCode === 200) {
		            console.log('下载成功');
					// this.saveFile(res.tempFilePath)
					// store.commit('toDownloadProgress',0) 
		        }else{
					uni.showToast({
						title:"下载失败，请检查网络",
						icon:"none"
					})
				}
		    },
			fail: function (res) {
				uni.showToast({
					title:"下载失败，请检查网络",
					icon:"none"
				})
			},
		});
		// 监听进度
		downloadTask.onProgressUpdate((res) => {
			// setTimeout(()=>{
			// 	store.commit('toDownloadProgress',res.progress)   //存储进度
			// },100)
		});
	},
	/**
	 * 文件下载本地永久储存
	 */
	saveFile(Path){
		uni.saveFile({
			tempFilePath: Path,
			success: function (res) {
				console.log(res)
				const savedFilePath = res.savedFilePath;
				store.commit('toFileUrl',savedFilePath)  //存储路径
			},
			fail: function (res) {
				console.log(res)
				uni.showToast({
					title:"下载失败！，请稍后重试",
					icon:"none"
				})
			},
		});
	},
	/**
	 * 文件下载
	 * url     			string    // 下载地址  
	 * fileType   	 	string    // 文件下载类型（例如:png,text）
	 * realFileName   	string 	  //  文件名称
	 */
	// 
	downloadFile(url,fileType,realFileName) {
		// ios 和 安卓分开
		if(uni.getSystemInfoSync().platform == 'ios'){
			this.uni_downloadFile(url,fileType,realFileName)
		}else{
			try{
				uni.showLoading({
					title:"正在下载中"
				})
				// 下载地址
				let wgtUrl = url;
				// 安装包的保存路径
				// let savePath = "_downloads/update/";
				let savePath = "file://storage/emulated/0/新建文件夹/" + realFileName + '.' + fileType;
				// 创建下载任务
				var dtask = plus.downloader.createDownload(wgtUrl, {
					filename: savePath
				}, function(d, status) {});
				// 添加监听器
				dtask.addEventListener("statechanged", this.onStateChanged, false);
				// 开始下载
				dtask.start();
			}catch(e){
				uni.hideLoading()
				uni.showToast({
					title:"下载失败！，请稍后重试",
					icon:"none"
				})
			}
		}
		
		
	},
	/**
	 * 下载任务的监听器
	 * @param {Object} dtask
	 * @param {Object} status
	 */
	onStateChanged(dtask, status) {
		var that = this
		if (dtask.state == 4 && status == 200) {
			// store.commit('toFileUrl',dtask.filename)  //存储路径
			plus.runtime.openFile(dtask.filename, function(error) {});  //文件下载完成自动打开文件
			uni.hideLoading()
			let d = plus.io.convertLocalFileSystemURL(dtask.filename)  // 存储路径
			// store.commit('toRealFileUrl',d)  //存储路径
		} else {
			let totalSize = dtask.totalSize;
			// 获得已下载的大小
			let downloadedSize = dtask.downloadedSize;
			// 计算进度
			let dp = ((downloadedSize / totalSize) * 100).toFixed(0);
			// console.log(dp)
			let downloadProgress = dp == 'NaN' ? 0 : dp;
			let d = Number(downloadProgress)
			// store.commit('toDownloadProgress',d)   //存储进度
			
		}
	},
	/**
	 * 打开文件夹
	 * name     string    // 下载到本地的文件路径  
	 */
	openFile(name){
		if(uni.getSystemInfoSync().platform == 'ios'){
			uni.openDocument({
				filePath: name,
				success: function (res) {
				  uni.hideLoading()
				},
				fail: function (res) {
				  uni.hideLoading()
				  uni.showToast({
				  	title:"文件类型不支持，请自行打开",
				  	icon:"none"
				  })
				},
			});
		}else{
			plus.runtime.openFile(name, function(error) {
				// console.log("open file failed: " + error);
			});
		}
	}	
		
}