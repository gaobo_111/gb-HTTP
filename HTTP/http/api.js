/**
 * 全局请求接口列表
 * Created by gaobo on 2020/08/08
 * 
 */
import http from "./http.js"
// 接口
export default {
	// 列表
	hotList(params) {
		return http.get("app/game/gameList.do", params)
	},
	// 这是个模拟登录的接口
	toLogin(params) {
		return http.post("app/login/login.do", params)
	},
	// 获取用户信息
	getloginuser(params) {
		return http.post("app/login/getLoginUser.do", params)
	},
	//租客订单
	toorder(params) {
		return http.post(`app/accountOrder/myOrder4Enants2.do`, params)
	},
	//退出登录
	outlogin() {
		return http.post("app/login/logout.do?equipmentUniqueness=" + '设备')
	},
	// 获取首页热门游戏列表
	hotList() {
		return http.post('app/game/gameList.do')
	},
	//获取一条舔狗日记
	getDoglickingDiary(params) {
		return http.get('https://api.wangpinpin.com/unAuth/getDoglickingDiary', params)
	},
	//获取一篇文章
	getEveryDayText() {
		return http.get('https://api.wangpinpin.com/unAuth/getEveryDayText')
	},
	//获取一篇诗词https://v2.jinrishici.com/one.json / https://api.lolicon.app/setu/
	Getjinrishici() {
		return http.get('https://tophub.today/n/mproPpoq6O')
	},
	//获取一个骂人信息
	Getzuan() {
		return http.get('https://nmsl.shadiao.app/api.php?level=min&lang=zh_cn')
	},
	//聊天
	GetAi(params) {
		return http.get('http://www.liulongbin.top:3006/api/robot',params)
	},
	//聊天文字变成语音
	Getsynthesize(text) {
		return http.get('http://www.liulongbin.top:3006/api/synthesize?text='+ text)
	},
	//星座运势
	Getconstellation(params) {
		return http.get('https://datamuse.guokr.com/api/front/common/muse/constellation/v1/fortune',params)
	},
	//聊天纪录
	apiMsgList(params) {
		let {pageNum,pageSize} = params
		return new Promise((resolute, reject)=>{
				//延时一秒,模拟联网
				setTimeout(function() {
					try {
						let list = [];
						//模拟下拉加载更多记录
						for (let i = 0; i < pageSize; i++) {
							let msgId = (pageNum - 1) * pageSize + i + 2;
							let type = i%2==0 ? 0 : 1
							let newObj = {
								id: msgId,
								type,
								text: "内容: 下拉获取聊天记录" + msgId + ' ' + i
							};
							// 此处模拟只有3页的消息 (第2页只有3条)
							if(pageNum>=3 && i>=3){}else{
								list.unshift(newObj);
							}
						}
						//模拟接口请求成功
						resolute(list);
					} catch (e) {
						//模拟接口请求失败+
						reject(e);
					}
				}, 1000)
			})
		
	},



}
