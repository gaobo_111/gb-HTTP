import Vue from 'vue'
import App from './App'
import http from './http/http.js' //http 请求
import api from './http/api.js' //api
import cache from "common/cache.js" // 缓存
import toast from "common/toast.js" // toast提示
import {
	showModal
} from "common/modal.js" // 弹窗提示
import Vuex from "store/index.js" // vuex
import copys from "common/copy.js" // 复制
import util from "common/util.js" // 常用方法
import constData from "common/constData.config.js" //颜色主题
import json from './json' //测试数据
import MescrollBody from "@/components/mescroll-uni/mescroll-body.vue" // 注册全局下拉刷新组件
import MescrollUni from "@/components/mescroll-uni/mescroll-uni.vue"
import tip from "@/components/tui-tips/tui-tips.vue" // tip
import loading from '@/components/loading/loading.vue' // 预加载
import lazyLoad from '@/components/lazy-load/lazy-load.vue' // 图片懒加载
import tabbar from '@/components/tabbar/u-tabbar.vue' // 自定义底部tabbar
import gbPickerArea from '@/components/gb-pickerArea/gb-pickerArea.vue' // 地区选择器多选
import gbPickerAreaSingle from '@/components/gb-pickerArea/gb-pickerArea-single.vue' // 地区选择器单选
import uniDrawerBottom from '@/components/uni-drawer/uni-drawer-bottom.vue' // 底部抽屉

// #ifndef VUE3
// 设置全局变量
Vue.prototype.$http = http;
Vue.prototype.$api = api;
Vue.prototype.$cache = cache
Vue.prototype.$toast = toast.showToast
Vue.prototype.$tip = toast.tip
Vue.prototype.$msg = toast.msg
Vue.prototype.$showModal = showModal
Vue.prototype.$store = Vuex
Vue.prototype.$commit = Vuex.commit
Vue.prototype.$stores = Vuex.store
Vue.prototype.$constData = constData
Vue.prototype.$copy = copys.onCopy
Vue.prototype.$util = util
Vue.prototype.$json = json

Vue.component('mescroll-body', MescrollBody)
Vue.component('mescroll-uni', MescrollUni)
Vue.component('tip', tip)
Vue.component('loading', loading)
Vue.component('lazyLoad', lazyLoad)
Vue.component('tabbar', tabbar)
Vue.component('gb_pickerArea', gbPickerArea)
Vue.component('gb_pickerArea_single', gbPickerAreaSingle)
Vue.component('uni-drawer-bottom', uniDrawerBottom)


//将themeColor混入this对象中
Vue.mixin({
	computed: {
		themeColor: {
			get() {
				return Vuex.getters.themeColor;
			},
			set(val) {
				Vuex.state.themeColor = val;
			}
		}
	}
});
Vue.config.productionTip = false
App.mpType = 'app'
const app = new Vue({
	...App
})
app.$mount()
// #endif

// #ifdef VUE3
import {
	createSSRApp
} from 'vue'
export function createApp() {
	const app = createSSRApp(App)
	app.use(MescrollBody).use(MescrollUni).use(tip).use(loading).use(lazyLoad).use(tabbar).use(gbPickerArea).use(gbPickerAreaSingle).use(uniDrawerBottom)
	app.config.globalProperties.$http = http;
	app.config.globalProperties.$api = api;
	app.config.globalProperties.$cache = cache
	app.config.globalProperties.$toast = toast.showToast
	app.config.globalProperties.$tip = toast.tip
	app.config.globalProperties.$msg = toast.msg
	app.config.globalProperties.$showModal = showModal
	app.config.globalProperties.$store = Vuex
	app.config.globalProperties.$commit = Vuex.commit
	app.config.globalProperties.$stores = Vuex.store
	app.config.globalProperties.$constData = constData
	app.config.globalProperties.$copy = copys.onCopy
	app.config.globalProperties.$util = util
	app.config.globalProperties.$json = json
	return {
		app
	}
}
// #endif
