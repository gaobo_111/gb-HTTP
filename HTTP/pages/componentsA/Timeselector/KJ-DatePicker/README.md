# KJ-DatePicker
文本自动滚动、广告牌

## 引入插件

```javascript
import DatePicker from '@/components/KJ-DatePicker/KJ-DatePicker.vue'
```

## 使用 
```
<DatePicker :dateType="'yq'" :isShow="isShowDatePicker" @close="DatePickerClose" @determineClick="DatePickerDetermineClick"
		 :beginYear="1995" :beginMonth="3" :beginDay="13" :beginHour="6" :beginQuarter="1" :beginWeek="1"
		 :endYear="2020" :endMonth="6" :endDay="24" :endHour="24" :endQuarter="2"  :endWeek="50"
		 :selectYear="2020" :selectMonth="6" :selectDay="24" :selectHour="24" :selectQuarter="2" :selectWeek="2" >
		</DatePicker>
```
