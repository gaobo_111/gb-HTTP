import Vue from 'vue'
import Vuex from 'vuex'
import cache from '@/common/cache.js'
import $constData from "@/common/constData.config.js"

Vue.use(Vuex)

const store = new Vuex.Store({
	state: {
		hasLogin:false, // 判断是否登录
		userInfo:{},
		name:"HTTP",
		themeColor: "",//主题
		// 消息提示
		tip:{
			msg: "内容",
			duration: 1500,
			type: "danger",
			position:"center"
		},
		closeStatippage:true, // 开启启动页
		tabIndex:-1, // swiper 列表的索引
		// 自定义tabbar数据
		vuex_tabbar: [{
				iconPath: "/static/tab/index.png",
				selectedIconPath: "/static/tab/index2.png",
				text: '首页',
				pagePath: '/pages/index/index'
			},
			{
				iconPath: "/static/tab/moban.png",
				selectedIconPath: "/static/tab/moban2.png",
				text: '模板',
				pagePath: '/pages/template/template'
			},
			{
				iconPath: "/static/tab/js.png",
				selectedIconPath: "/static/tab/js2.png",
				text: 'JS',
				pagePath: '/pages/js/js'
			},
			{
				iconPath: "/static/tab/user.png",
				selectedIconPath: "/static/tab/user2.png",
				text: '我的',
				pagePath: '/pages/user/user',
				
			}
		],
		recordTime:{},// 记录播放时间
	},
	// mutations对象
	// 	包含多个方法可以直接更新state
	// 	一个方法就是一个mutation
	// 	mutation只能包含更新state的同步代码，不会有逻辑
	// 	mutation由action触发调用：(commit(mutationName))
	mutations: {
		// 提示
		tipStatus(state,status){
			state.tip = status
		},
		// tab列表
		tabSwiper(state,status){
			state.tabIndex = status
		},
		// 登录
		login(state, provider) {
			state.hasLogin = true;
			state.userInfo = provider;
			cache.updateCache(cache.USERINFO,provider)
		},
		// 退出登录
		logout(state) {
			state.hasLogin = false;
			state.userInfo = {};
			cache.removeCache(cache.USERINFO)
		},
		// 开启关闭启动页
		toCloseStatippage(state,status){
			state.closeStatippage = status;
		},
		// 设置主题
		setThemeColor(state, provider) {
			state.themeColor = provider;
			cache.updateCache(cache.THEMECOLOR, provider)
		},
		// 记录播放的时间
		toRecordTime(state, e){
			state.recordTime = e;
		}
	},
	/**
	 * 	getters对象
		包含多个get计算属性的方法
	 */
	getters: {
		// 全局配置
		themeColor: state => {
			let theme = state.themeColor || cache.fetchCache(cache.THEMECOLOR)
			if (!theme) {
				theme = $constData.themeList[0]
			}
			return theme;
		},
	},
	/**
		actions对象
		包含多个方法通过触发mutation调用 间接更新state
		一个方法就是一个action
		执行异步代码
		action 通过组件触发调用：this.$store.dispatch('actionName')
	 */
	actions: {
		 tipStatu(context,payload){
			context.commit("tipStatus",payload)
		}
	}
})

export default store
