# gb-HTTP

#### 介绍
HTTP请求

#### 软件架构
```properties

	├─doc                                     	
	│  ├─小技巧									# 日常生活中代码技巧文档
	│  └─README									# 代码结构
	└─source								  	# app源码	
		├─common							  	# js插件
		│  ├─cache        			  	  		# 缓存
		│  ├─modal        			  	  		# 弹窗
		│  ├─qqmap-wx-jssdk.min        			# 
		│  ├─toast        						# 提示
		├─components                          	# 插件库
		│  ├─app-tab       			  			# tab选项
		├─http                          		# http请求
		│  ├─     			  					# 
		├─mixin                          		# mixin混入
		│  ├─     			  					# 
		├─pages                          		# 页面
		│  ├─     			  					# 
		├─static                          		# 静态资源
		│  ├─     			  					# 
		├─store                          		# vuex
		│  ├─     			  					# 
		├─unpackage                          	# 
		├─App                          			# 
		├─main                          		# 
		├─manifest                          	# 
		├─pages                          		# 
		├─uni                          			# 
    
```


#### 安装教程

# 第一步，项目引入http文件夹

# 第二步，在main.js文件注册
`import http from './http/http.js'`
`import api from './http/api.js'`
`Vue.prototype.$http = http;`
`Vue.prototype.$api = api`

# 第三步，js用法

参数 并配置baseUrl地址等相关
api.js用法

参数 : params 
url : app/login/xxxxxx.do  
函数名： xxxx
login:(params)=>{
	return http.post("app/login/xxxxxx.do",params)
},

# 第四步，页面引入用法
login(){
	let params = {      // 传递参数/不需要传参可不写
		username:"ggl",   
	}
	this.$api.login(params).then(res => {  // 成功的回调
		例：
		const {result,info,data} = res.data
		if(result === 1){
			
		}
    }).catch((err) => {  // 错误的回调
	}).finally((e) => {  // 失败成功都会调用的回调
	}) 
},

#### 使用说明
## 参数
baseUrl ：请求地址




#### 意见

微信小程序相关插件请参考作者码云地址


#### 参与贡献




#### 码云特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5.  码云官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
